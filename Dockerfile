FROM registry.gitlab.com/jbmevel/flum_aesi/jenga

WORKDIR /app

COPY requirements.txt ./requirements-app.txt
RUN pip install --no-cache-dir -r ./requirements-app.txt
RUN pip install torch==1.8.1+cpu torchvision==0.9.1+cpu torchaudio===0.8.1 -f https://download.pytorch.org/whl/torch_stable.html

COPY ./src/app/ ./app
CMD ["python", "-um", "onda.main", "-ervvp", "app.pipeline:pipe"]