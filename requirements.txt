pillow
tensorflow
tensorboard
keras
matplotlib
# nomkl==1.0 #conda-forge
# Last version on pytorch.org
# pip install torch==1.7.1+cpu torchvision==0.8.2+cpu -f https://download.pytorch.org/whl/torch_stable.html
# torch==1.7.1+cpu
# torchvision==0.8.2+cpu
pandas
pytorch_lightning
opencv-python
sklearn
seaborn
boto3
xgboost
