from app.utils import ImageFolderWithPaths

from onda.pipeline.job import Output, Input
from onda.pipeline.job import pipeline_job
from onda.storage.s3 import S3StorageService
from onda.storage.file import FileStorageService
from torchvision import transforms
from PIL import Image
import numpy as np
import os
import pickle


# @pipeline_job(Input('raw_images'),
#               Output('predictions_v1'))
# def diamant_classifier_test_v1(images, predictions):
#     # METHODE 1: induit de changer les fonctions génératives utilisées (ImageFolder... et DataLoader)
#
#     # initialize storage service
#     s3_service = S3StorageService([])
#
#     # Get all images paths in input folder
#     image_paths = s3_service.ls(glob=images.logical_path)
#
#     # Open images files and convert to desired image format
#     # the "with s3_service.open" statement is equivalent to : img = s3_service.read_from_s3(path, filetype='png')
#     for path in image_paths:
#         with s3_service.open(path, mode='r') as img:
#             print(np.array(Image.open(img)))  # utiliser un tenseur plutôt
#
#     # générer le tuple img, label, path à la main puis dérouler le test


# @pipeline_job(Output('predictions_v2'))
# def diamant_classifier_test_v2(predictions):
#
#     # METHODE 2: induit d'attacher un 'volume' au container Docker pour avoir
#     # les données accessibles dans le système de fichiers
#
#     # get images directory full path
#     images_path = os.path.join(os.getenv('FILE_STORAGE_PATH'), 'raw_images')
#
#     input_size = 224
#     data_transforms = transforms.Compose([
#         transforms.Resize(input_size),
#         transforms.ToTensor(),
#     ])
#     image_dataset = ImageFolderWithPaths(images_path, transform=data_transforms)
#     print(image_dataset)
#     # ...


@pipeline_job(model_v1=Output('/xgb_best.sav'))
def write_model(model_v1):
    model_path = os.path.join(os.getenv('FILE_STORAGE_PATH'), model_v1.logical_path.split('/')[1])
    print(model_path)
    model_v1.write(pickle.load(open(model_path, 'rb')))


@pipeline_job(model_v1=Input('/xgb_best.sav'),
              model_v2=Output('/xgb_best_bis.sav'))
def read_write_model(model_v1, model_v2):
    print("reading model")
    model_temp = model_v1.read()[0]
    print(type(model_temp))
    print("writing model")
    model_v2.write(model_temp)