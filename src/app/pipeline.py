from app import features
from onda.pipeline.onda.publisher import OndaPipelinePublisher, CronPipelineScheduler, EventPipelineScheduler


pipe = OndaPipelinePublisher(default_storage='file')
pipe.discover(features)
# pipe.schedule(CronPipelineScheduler(['predictions_v2'], '*/2 * * * *'))
pipe.schedule(CronPipelineScheduler(['/xgb_best.sav'], '*/2 * * * *'))
pipe.schedule(EventPipelineScheduler(['/xgb_best_bis.sav']))