import numpy as np
from onda.storage.s3 import S3StorageService, S3Open
import boto3
from botocore.exceptions import ClientError
import os
from io import BytesIO
from PIL import Image


# credentials
BUCKET_NAME = os.getenv('BUCKET_NAME', 'staging-onda')
AWS_ACCESS_KEY_ID = os.getenv('AWS_ACCESS_KEY_ID', None)
AWS_SECRET_ACCESS_KEY = os.getenv('AWS_SECRET_ACCESS_KEY', None)
AWS_ENDPOINT_URL = os.getenv('AWS_ENDPOINT_URL', None)
s3Client = boto3.client('s3',
                        aws_access_key_id=AWS_ACCESS_KEY_ID,
                        aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                        endpoint_url=AWS_ENDPOINT_URL)


ss = S3StorageService(["s3://staging-onda/covid19"])
paths = ss.ls(glob="covid19/france/utils/")
paths_list = [p for p in paths]
print(paths_list)
with S3Open(paths_list[1], mode='r', s3client=s3Client) as content:
    print(content)
    print(np.array(Image.open(content)))